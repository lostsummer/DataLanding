#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
min2mysql

Usage:
  min2mysql -h | --help | --version
  min2mysql [-d <date>]

Options:
  -h --help         show help
  -v --version      show version
  -d <date>         specified date if not today
"""

import json
import redis
from sqlalchemy import *
from sqlalchemy.orm import *
from time import *
from datetime import datetime
from config import config
from dfparse.dfparse import *
from docopt import docopt


__author__ = "wangyx"
__version__ = "0.5.1"


def file2db(filename):
    dfdir = config['datafile']['dir']
    dfpath = '{dir}/{file}'.format(dir=dfdir, file=filename)
    datafile = DataFile(dfpath, Day)
    for goodsid in datafile.goodsidx:
        emcode = '{0:07}'.format(goodsid)
        cm = codemaps.get(emcode, {})
        group = cm.get("category", [0]).pop()
        if group in config["block_group"]:
            continue

        code = cm.get("code", emcode[1:])
        name = cm.get("name", "")
        ts = datafile.getgoodstms(goodsid)
	inlist = []
        for min in ts:
            time = min.time + 200000000000
            if datenum == time // 10000:
                row = {}
                year = time // 100000000
                month = (time // 1000000) % 100
                day = (time // 10000) % 100
                hour = (time % 10000) // 100
                minute = time % 100
                row["time"] = datetime(year, month, day, hour, minute)
                row["emcode"] = goodsid
                row["code"] = code
                row["name"] = name
                row['open'] = min.open / 1000.0
                row['high'] = min.high / 1000.0
                row['low'] = min.low / 1000.0
                row['close'] = min.close/1000.0
                row['volume'] = min.volume
                row['amount'] = min.amount
                inlist.append(row)

        if len(inlist) > 0:
            session.execute(
                kline_table.insert(),
                inlist)
            session.commit()
            print("insert code: {0}, emcode: {1}".format(code, emcode))
            global ingoodssum
            ingoodssum += 1

    return


def getCodeMap(redis_cli):
    cg = {}
    for i in redis_cli.hscan_iter("codemap:codes"):
        k = i[0]
        v = json.loads(i[1])
        cg[k] = v
    return cg



if __name__ == '__main__':
    arguments = docopt(__doc__, version='min2mysql {}'.format(__version__))
    datestr = arguments['-d']
    if not datestr:
        datestr = '{}'.format(strftime("%Y%m%d"))

    datenum = int(datestr)
    linkstr = 'mysql://{user}:{password}@{host}/{db}?charset=utf8'.format(
                                      user=config["mysql"]["user"],
                                      password=config["mysql"]["password"],
                                      host=config["mysql"]["host"],
                                      db=config["mysql"]["db"])

    # 按月分表 
    ym = datestr[:-2]
    table = 'min1_{}'.format(ym)

    engine = create_engine(linkstr)
    metadata = MetaData(engine)
    kline_table = Table(table, metadata,
                            Column('emcode', INT, primary_key=True),
                            Column('code', VARCHAR(16)),
                            Column('name', VARCHAR(64)),
                            Column('time', DATETIME, primary_key=True),
                            Column('high', DECIMAL(19, 3)),
                            Column('open', DECIMAL(19, 3)),
                            Column('low', DECIMAL(19, 3)),
                            Column('close', DECIMAL(19, 3)),
                            Column('volume', BIGINT),
                            Column('amount',  BIGINT))

    metadata.create_all()
    makeSession = sessionmaker(engine)
    code_cli = redis.Redis(host=config["redis"]["host"],
                            port=config["redis"]["port"],
                            db=config["redis"]["dbcode"])

    codemaps = getCodeMap(code_cli)
    session = makeSession()
    ingoodssum = 0
    file2db('Min1.dat')
    file2db('Min1_HK.dat')

    print("insert goods total: {}".format(ingoodssum))
    session.close()
