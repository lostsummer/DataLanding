#!/usr/bin/env python
# -*- coding: utf-8 -*-

config = {
    "redis":{
        "host":"192.168.8.211",
        "port":6379,
        "dbmin":0,
        "dbcode":0
    },
    "datafile":{
        "dir":"/usr/local/EMoney/Data"
    },
    "mysql":{
        "host":"192.168.42.83",
        "user":"pywriter",
        "password":"emoney",
        "db":"kline"
    },
    "mssql":{
        "host":"192.168.42.90",
        "user":"sa",
        "password":"sa123321~",
        "db":"EmHqSave"
    },
    "hdf5":{
        "dir":"/data/min1/hdf5"
    },
    "block_group":[
        140, 160, 170, 180, 190, 200, 999
    ]
}



