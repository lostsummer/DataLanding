#!/usr/bin/env python

"""docstring
"""

__revision__ = '0.1'
__author__ = 'wangyx'


import redis
import json
import os
import commands
from time import *
import pandas as pd
from config import config

stationName = ['SH', 'SZ', 'BK',
               'AH', 'GZ', 'HK',
               'WI', 'SP', 'WP',
               'US', 'US', 'WH']

def getGoodsRedisData(redis_cli, key):
    res =  redis_cli.lrange(key, 0, -1)
    timestamps = []
    values = {
                'open': [],
                'high': [],
                'low': [],
                'close': [],
                'amount': [],
                'volume': [],
             }
    for i in res:
        d = json.loads(i)
        timestamps.append(str(d['time']))
        values['open'].append(d['open'])
        values['high'].append(d['high'])
        values['low'].append(d['low'])
        values['close'].append(d['close'])
        values['amount'].append(d['amount'])
        values['volume'].append(d['volume'])

    times = pd.DatetimeIndex(timestamps)
    return pd.DataFrame(values, index=times)


def getMin1Keys(redis_cli):
    keys = []
    for i in redis_cli.scan_iter(match="min1*"):
        keys.append(i)
    return keys


def getCodeGroup(redis_cli):
    cg = {}
    cc = {}
    for i in redis_cli.hscan_iter("codemap:codes"):
        k = i[0]
        data = json.loads(i[1])
        vg = data["category"][0]
        vc = data["code"]
        cg[k] = vg
        cc[k] = vc
    return cg, cc


if __name__ == "__main__":
    min1_cli = redis.Redis(host=config["redis"]["host"],
                            port=config["redis"]["port"],
                            db=config["redis"]["dbmin"])

    code_cli = redis.Redis(host=config["redis"]["host"],
                            port=config["redis"]["port"],
                            db=config["redis"]["dbcode"])

    codegroup, codecode = getCodeGroup(code_cli)
    min1_keys = getMin1Keys(min1_cli)

    hdfname = "min1_{}.h5".format(strftime("%Y%m%d"))
    subdir = "{}".format(strftime("%Y%m"))
    hdfdir = config["hdf5"]["dir"] + "/" + subdir
    if not os.path.exists(hdfdir):
        os.makedirs(hdfdir)

    hdfpath = hdfdir + "/" + hdfname
    hdfstore = pd.HDFStore(hdfpath)
    for k in min1_keys:
        emcode = k[5:]
        stno = int(emcode)//1000000
        if stno > 11:
            station = 'QQ'
        else:
            station = stationName[stno]
        group = codegroup.get(emcode, 999)
        if group in config["block_group"]:
            continue

        code = codecode.get(emcode, '')
        if code == '':
            code = '{0:06}'.format(int(emcode)%1000000)

        f = getGoodsRedisData(min1_cli, k)
        hdfstore["{0}_{1}".format(station, code)] = f
    hdfstore.close()
    s, _ = commands.getstatusoutput('gzip {}'.format(hdfpath))
    exit(s)
