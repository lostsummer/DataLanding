#!/usr/bin/env python
# -*- coding: utf-8 -*-

__revision__ = '0.1'


from sqlalchemy import *
from sqlalchemy.orm import *
from config import config
from time import *
from datetime import datetime
from config import config
import json
import redis

def getGoodsRedisData(redis_cli, key):
    res =  redis_cli.lrange(key, 0, -1)
    emcode = int(key[5:])
    gdatas = []
    for i in res:
        d = json.loads(i)
        gdatas.append({"time":d["time"],
                       "emcode":emcode,
                       "open":d["open"],
                       "high":d["high"],
                       "low":d["low"],
                       "close":d["close"],
                       "amount":d["amount"],
                       "volume":d["volume"]})
    return gdatas


def getMin1Keys(redis_cli):
    keys = []
    for i in redis_cli.scan_iter(match="min1*"):
        keys.append(i)
    return keys


def getCodeMap(redis_cli):
    cg = {}
    for i in redis_cli.hscan_iter("codemap:codes"):
        k = i[0]
        v = json.loads(i[1])
        cg[k] = v
    return cg



#def insertToDB(sq, goods, code, name):
def insertToDB(session, goods, code, name):
    if code == '0':
	return

    inlist = []
    for i in goods:
        itime = i["time"]
        year = itime // 100000000
        month = (itime // 1000000) % 100
        day = (itime // 10000) % 100
        hour = (itime % 10000) // 100
        minute = itime % 100
        i["time"] = datetime(year, month, day, hour, minute)
        i["code"] = code
        i["name"] = name
        inlist.append(i)

    #session = sq.get()
    session.execute(
        kline_table.insert(),
        inlist)
    session.commit()
    #sq.put(session)
    print("after commit  {}".format(code))



linkstr = 'mysql://{user}:{password}@{host}/{db}?charset=utf8'.format(
                                  user=config["mysql"]["user"],
                                  password=config["mysql"]["password"],
                                  host=config["mysql"]["host"],
                                  db=config["mysql"]["db"])

# 按月分表 
ym = strftime("%Y%m")
table = 'min1_{}'.format(ym)

engine = create_engine(linkstr)
metadata = MetaData(engine)
kline_table = Table(table, metadata,
                        Column('emcode', INT, primary_key=True),
                        Column('code', VARCHAR(16)),
                        Column('name', VARCHAR(64)),
                        Column('time', DATETIME, primary_key=True),
                        Column('high', DECIMAL(19, 3)),
                        Column('open', DECIMAL(19, 3)),
                        Column('low', DECIMAL(19, 3)),
                        Column('close', DECIMAL(19, 3)),
                        Column('volume', BIGINT),
                        Column('amount',  BIGINT))

metadata.create_all()

makeSession = sessionmaker(engine)

from multiprocessing import Pool, Manager

min1_cli = redis.Redis(host=config["redis"]["host"],
			port=config["redis"]["port"],
			db=config["redis"]["dbmin"])

code_cli = redis.Redis(host=config["redis"]["host"],
			port=config["redis"]["port"],
			db=config["redis"]["dbcode"])

codemaps = getCodeMap(code_cli)
min1_keys = getMin1Keys(min1_cli)

session = makeSession()
for k in min1_keys:
    emcode = k[5:]
    cm = codemaps.get(emcode, {})
    group = cm.get("category", [0]).pop()
    if group in config["block_group"]:
        continue

    goods = getGoodsRedisData(min1_cli, k)
    code = cm.get("code", "0")
    name = cm.get("name", "")
    insertToDB(session, goods, code, name)

session.close()
