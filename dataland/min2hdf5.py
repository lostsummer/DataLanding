#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
min2hdf5

Usage:
  min2hdf5 -h | --help | --version
  min2hdf5 [-d <date>]

Options:
  -h --help         show help
  -v --version      show version
  -d <date>         date of today, write to db1
"""

import os
import sys, commands
import redis, json
import pandas as pd
from docopt import docopt
from time import *
from dfparse.dfparse import *
from config import config

__author__ = "wangyx"
__version__ = "0.5.1"

stationName = ['SH', 'SZ', 'BK',
               'AH', 'GZ', 'HK',
               'WI', 'SP', 'WP',
               'US', 'US', 'WH']

def gethdfpath(datestr):
    hdfname = "min1_{}.h5".format(datestr)
    subdir = datestr[:-2]
    hdfdir = config["hdf5"]["dir"] + "/" + subdir
    if not os.path.exists(hdfdir):
        os.makedirs(hdfdir)

    hdfpath = hdfdir + "/" + hdfname
    return hdfpath

def formatrow(min):
    data = {
        'time':min.time + 200000000000,
        'open':min.open/1000.0,
        'high':min.high/1000.0,
        'low':min.low/1000.0,
        'close':min.close/1000.0,
        'volume':min.volume,
        'amount':min.amount
    }
    return data


def getCodeGroup(redis_cli):
    cg = {}
    cc = {}
    for i in redis_cli.hscan_iter("codemap:codes"):
        k = i[0]
        data = json.loads(i[1])
        vg = data["category"][0]
        vc = data["code"]
        cg[k] = vg
        cc[k] = vc
    return cg, cc



def writehdf(filename):
    dfdir = config['datafile']['dir']
    dfpath = '{dir}/{file}'.format(dir=dfdir, file=filename)
    datafile = DataFile(dfpath, Day)
    for goodsid in datafile.goodsidx:
        emcode = '{0:07}'.format(goodsid)
        group = codegroup.get(emcode, 999)
        if group in config["block_group"]:
            continue

        code = codecode.get(emcode, '')
        if code == '':
            code = '{0:06}'.format(goodsid%1000000)
        stno = goodsid//1000000
        if stno > 11:
            station = 'QQ'
        else:
            station = stationName[stno]

        timestamps = []
        values = {
                    'open': [],
                    'high': [],
                    'low': [],
                    'close': [],
                    'amount': [],
                    'volume': [],
                 }

        ts = datafile.getgoodstms(goodsid)
        for min in ts:
            time = min.time + 200000000000
            if datenum == time // 10000:
                timestamps.append(str(time))
                values['open'].append(min.open / 1000.0)
                values['high'].append(min.high / 1000.0)
                values['low'].append(min.low / 1000.0)
                values['close'].append(min.close/1000.0)
                values['volume'].append(min.volume)
                values['amount'].append(min.amount)

        times = pd.DatetimeIndex(timestamps)
        dataframe = pd.DataFrame(values, index=times)
        key = '{0}_{1}'.format(station, code)
        hdfstore[key] = dataframe
        print('write emcode: {0}, key: {1}'.format(emcode, key))
        global wrgoodssum
        wrgoodssum += 1
    return


if __name__ == '__main__':
    arguments = docopt(__doc__, version='min2hdf5 {}'.format(__version__))
    datestr = arguments["-d"]
    if not datestr:
        datestr = "{}".format(strftime("%Y%m%d"))

    datenum = int(datestr)
    hdfpath = gethdfpath(datestr)
    hdfstore = pd.HDFStore(hdfpath)
    code_cli = redis.Redis(host=config["redis"]["host"],
                            port=config["redis"]["port"],
                            db=config["redis"]["dbcode"])

    codegroup, codecode = getCodeGroup(code_cli)
    wrgoodssum = 0
    writehdf('Min1.dat')
    writehdf('Min1_HK.dat')
    hdfstore.close()
    print("total write goods: {}".format(wrgoodssum))
    s, _ = commands.getstatusoutput('gzip {}'.format(hdfpath))
    exit(s)
