#!/usr/bin/env python

"""docstring
"""

__revision__ = '0.1'
__author__ = 'wangyx'


import redis
import pymssql
import json
from config import config

def getGoodsRedisData(redis_cli, key):
    res =  redis_cli.lrange(key, 0, -1)
    emcode = int(key[5:])
    gdatas = []
    for i in res:
        d = json.loads(i)
        gdatas.append({"time":d["time"],
                       "emcode":emcode,
                       "open":d["open"],
                       "high":d["high"],
                       "low":d["low"],
                       "close":d["close"],
                       "amount":d["amount"],
                       "volume":d["volume"]})
    return gdatas


def getMin1Keys(redis_cli):
    keys = []
    for i in redis_cli.scan_iter(match="min1*"):
        keys.append(i)
    return keys


def getCodeGroup(redis_cli):
    cg = {}
    for i in redis_cli.hscan_iter("codemap:codes"):
	k = i[0]
	data = json.loads(i[1])
	v =  data["category"][0]
	cg[k] = v
    return cg


def commitGoodsByProc(cli, goods):
    strlist = []
    for i in goods:
	procstr = """
exec dbo.InsertOrUpdataMin1 {time}, {low}, {close}, {high}, {open}, {volume}, {amount}, {emcode}
""".format(time=i["time"],
           emcode=i["emcode"],
           open=i["open"],
           high=i["high"],
           low=i["low"],
           close=i["close"],
           volume=i["volume"],
           amount=i["amount"])
        strlist.append(procstr)
    batstr = ''.join(strlist)
    with cli.cursor() as c:
        c.execute(batstr)
        cli.commit()


def insetToDB(clique, lock, goods):
    lock.acquire()
    if clique.empty():
        cli = getConnectedMssqlCli()
    else:
        cli = clique.get_nowait()
    lock.release()
    commitGoodsByProc(cli, goods)
    clique.put_nowait(cli)


def getConnectedMssqlCli():
    cli =  pymssql.connect(server=config["mssql"]["host"],
                               user=config["mssql"]["user"],
                               password=config["mssql"]["password"],
                               database=config["mssql"]["db"],
                               login_timeout=3)
    return cli


if __name__ == "__main__":
    min1_cli = redis.Redis(host=config["redis"]["host"],
                            port=config["redis"]["port"],
                            db=config["redis"]["dbmin"])

    code_cli = redis.Redis(host=config["redis"]["host"],
                            port=config["redis"]["port"],
                            db=config["redis"]["dbcode"])

    codegroup = getCodeGroup(code_cli)
    min1_keys = getMin1Keys(min1_cli)

    from multiprocessing import Pool, Manager
    pool = Pool()
    manager = Manager()
    clique = manager.Queue(maxsize=12)
    lock = manager.Lock()

    for k in min1_keys:
	emcode = k[5:]
	group = codegroup.get(emcode, 0)
	if group in config["block_group"]:
            continue

        goods = getGoodsRedisData(min1_cli, k)
        print("commit {0}".format(emcode))
        pool.apply_async(insetToDB, args=(clique, lock, goods))

    pool.close()
    pool.join()
