# Redis 及 Min1.dat数据的每日存储转换

- SQLServer
	- r2sqlserver.py
- MySql
	- r2mysql.py
	- min2mysql.py
- HDF5
	- r2hdf5.py
	- min2hdf5.py

## r2sqlserver.py

黄金战法落库

测试使用SQLServer2016内存数据库，200万条记录大概三分写完

张涛为提供表结构和存储过程

### 表结构

```
INSERT INTO dbo.tb_min1 WITH(SNAPSHOT)
        ( time ,
          low ,
          [close] ,
          high ,
          [open] ,
          volume ,
          amount ,
          emcode
        )
VALUES  ( 0 , -- time - bigint
          NULL , -- low - decimal
          NULL , -- close - decimal
          NULL , -- high - decimal
          NULL , -- open - decimal
          0 , -- volume - bigint
          0 , -- amount - bigint
          0  -- emcode - int

```

### 调用存储过程

```
dbo.InsertOrUpdataMin1
   @p_time   BIGINT NOT NULL, 
   @p_low    DECIMAL(10,3),
   @p_close  DECIMAL(10,3),
   @p_high   DECIMAL(10,3),
   @p_open   DECIMAL(10,3),
   @p_volume BIGINT,
   @p_amount BIGINT,
   @p_emcode INT
```
